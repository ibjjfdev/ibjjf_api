module IbjjfApi
  class ApiError < StandardError
    attr_reader :message, :code

    def initialize(code, message)
      @code = code
      @message = message
    end

    def to_s
      "[HTTP #{code}] #{message}"
    end
  end
end
