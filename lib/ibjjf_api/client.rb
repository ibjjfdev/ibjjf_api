# Endpoints docs:
# https://hmg-api.ibjjfdb.com/api-docs/

# Pegar token:

# POST https://hmg-api.ibjjfdb.com/auth/security/oauth/token
# { "grant_type": "client_credentials", "client_id": "DF2C5A40-06A0-11E8-B136-2391F689CB12", "client_secret": "tSlQ3dZoRXSpzzjnRkhmPwgaOcqrmnRKlKIOL77gx" }

# O token é válido por 1 dia.
# Enviar o token no cabeçalho http para os outros endpoints
# Authorization: Bearer TOKEN.....

# GET  https://hmg-api.ibjjfdb.com/admin/championships/championships
# Retornar os campeonatos
# Pode ser filtrado via queryString por: name, status{active, inactive}, registrationStatus{active, inactive},
# dateStart e dateEnd

# GET  https://hmg-api.ibjjfdb.com/admin/championships/championships/{championship_id}/academiesResults
# Retornar os resultados das academias por categoria

# GET  https://hmg-api.ibjjfdb.com/admin/championships/championships/{championship_id}/prices
# Lista os preços dos ingressos para os campeonatos

# GET  https://hmg-api.ibjjfdb.com/admin/championships/championships/{championship_id}/prizes
# Lista as premiações dos campeonatos

# GET  https://hmg-api.ibjjfdb.com//admin/courses/courses/{course_id}/prices
# Lista os preços para inscrições nos cursos

# GET  https://hmg-api.ibjjfdb.com/admin/championships/championships/{championship_id}/competitors
# Retorna lista de competidores no campeonato
# Pode ser filtrado via queryString por: belts{belt_id}, genders{gender_id}, ageDivisions{age_divisions_ids}, weightDivisions{weight_divisions_ids}

# GET  https://hmg-api.ibjjfdb.com/admin/championships/championships/{championshipId}/coaches
# Retorna lista de coaches no campeonato
# Pode ser filtrado via queryString por: id, name, email

# GET https://hmg-api.ibjjfdb.com/admin/championships/championships/{championship_id}/divisions
# Retorna lista de categorias associadas ao campeonato

# GET https://hmg-api.ibjjfdb.com/admin/championships/championships/{championship_id}/divisions/tree
# Retorna um agrupamento por gênero das divisões de idade e faixas associadas a um campeonato

# GET https://hmg-api.ibjjfdb.com/admin/championships/championships/{championship_id}/results
# Retorna os resultados lançados para um campeonato

# GET https://hmg-api.ibjjfdb.com/admin/athletes/athletes
# Retorna a lista dos atletas
# Pode ser filtrado via queryString por: name, academy_id, membershipStatus e certificateStatus

# GET https://hmg-api.ibjjfdb.com/admin/athletes/athletes/athlete_id
# Retorna o atleta por id

# GET https://hmg-api.ibjjfdb.com/admin/academies/academies
# Retorna a lista das academias
# Pode ser filtrado via queryString por: academyRegistrationLastApprovedStatus e status

# GET https://hmg-api.ibjjfdb.com/admin/academies/academy_id
# Retorna a academia por id

# GET https://hmg-api.ibjjfdb.com/admin/academies/teams/academy_team_id
# Retorna um time de academia por id

# GET https://hmg-api.ibjjfdb.com//admin/academies/academies/searchNearby
# Retorna uma lista de academias próximas ao endereço enviado.
# Pode ser filtrado pelo endereço do usuário e a lista retornada é sempre ordenada por ordem de distância

# GET https://hmg-api.ibjjfdb.com/admin/athletes/memberships/documents
# Retorna a lista de documentos inerentes ao processo de membership de um atleta.

# GET https://hmg-api.ibjjfdb.com/admin/academies/registrations/pricingAndDocuments
# Retorna a lista de documentos inerentes ao processo de agremiação de uma academia.

# GET https://hmg-api.ibjjfdb.com/athleteCertificates/pricingAndDocuments
# Retorna a lista de documentos inerentes ao processo de diplomação de um atleta faixa preta.

# GET https://hmg-api.ibjjfdb.com/admin/countries
# Retorna uma lista de países

# GET https://hmg-api.ibjjfdb.com/admin/belts
# Retorna uma lista de faixas.
# Pode ser filtrado via queryString por: age

# GET https://hmg-api.ibjjfdb.com/admin/courses/courses
# Retorna uma lista de cursos.

require 'retryable'
require 'json'
require 'rest-client'
require 'addressable/uri'

module IbjjfApi
  class Client
    PAGE_SIZE = 500
    MAX_TRIES = 5

    attr_reader :auth_token, :client_id, :client_secret

    def initialize(host_url: nil, client_id: nil, client_secret: nil)
      @host_url = host_url || ENV['IBJJF_API_HOST']
      @client_id = client_id || ENV['IBJJF_API_CLIENT_ID']
      @client_secret = client_secret || ENV['IBJJF_API_CLIENT_SECRET']
    end

    def championship_info(championship_id)
      get "#{@host_url}/admin/championships/championships/#{championship_id}", {}
    end

    def list_championships(page_size = PAGE_SIZE, page = 1, filter_params = {})
      get "#{@host_url}/admin/championships/championships", { pageSize: page_size, page: page }.merge(filter_params)
    end

    def list_championship_competitors(championship_id, page_size = PAGE_SIZE, page = 1, filter_params = {})
      get "#{@host_url}/admin/championships/championships/#{championship_id}/competitors", { pageSize: page_size, page: page }.merge(filter_params)
    end

    def list_championship_prizes(championship_id)
      get "#{@host_url}/admin/championships/championships/#{championship_id}/prizes", {}
    end

    def list_athletes(page_size = PAGE_SIZE, page = 1, filter_params = {})
      get "#{@host_url}/admin/athletes/athletes", { pageSize: page_size, page: page }.merge(filter_params)
    end

    def get_athlete(athlete_id)
      get "#{@host_url}/admin/athletes/athletes/#{athlete_id}"
    end

    def list_academies(page_size = PAGE_SIZE, page = 1, filter_params = {})
      get "#{@host_url}/admin/academies/academies", { pageSize: page_size, page: page }.merge(filter_params)
    end

    def active_registration_academies(page_size = PAGE_SIZE, page = 1, filter_params = {})
      get "#{@host_url}/admin/academies/academies/activeRegistration", { pageSize: page_size, page: page }.merge(filter_params)
    end

    def search_nearby_academies(page_size = PAGE_SIZE, page = 1, filter_params = {})
      get "#{@host_url}/admin/academies/academies/searchNearby", { pageSize: page_size, page: page }.merge(filter_params)
    end

    def get_academy(academy_id)
      get "#{@host_url}/admin/academies/academies/#{academy_id}"
    end

    def get_academy_team(academy_team_id)
      get "#{@host_url}/admin/academies/teams/#{academy_team_id}"
    end

    def list_courses(page_size = PAGE_SIZE, page = 1, filter_params = {})
      get "#{@host_url}/admin/courses/courses", { pageSize: page_size, page: page }.merge(filter_params)
    end

    def get_course(course_id)
      get "#{@host_url}/admin/courses/courses/#{course_id}"
    end

    def list_course_prices(course_id)
      get "#{@host_url}/admin/courses/courses/#{course_id}/prices"
    end

    def list_championship_divisions(championship_id, page_size = PAGE_SIZE, page = 1)
      get "#{@host_url}/admin/championships/championships/#{championship_id}/divisions", { pageSize: page_size, page: page }
    end

    def list_championship_divisions_tree(championship_id, page_size = PAGE_SIZE, page = 1)
      get "#{@host_url}/admin/championships/championships/#{championship_id}/divisions/tree", { pageSize: page_size, page: page }
    end

    def list_championship_coaches(championship_id, page_size = PAGE_SIZE, page = 1)
      get "#{@host_url}/admin/championships/championships/#{championship_id}/coaches", { pageSize: page_size, page: page }
    end

    def list_championship_results(championship_id, page_size = PAGE_SIZE, page = 1)
      get "#{@host_url}/admin/championships/championships/#{championship_id}/results", { pageSize: page_size, page: page }
    end

    def list_championship_academies_results(championship_id, page_size = PAGE_SIZE, page = 1)
      get "#{@host_url}/admin/championships/championships/#{championship_id}/academiesResults", { pageSize: page_size, page: page }
    end

    def list_championship_prices(championship_id, page_size = PAGE_SIZE, page = 1)
      get "#{@host_url}/admin/championships/championships/#{championship_id}/prices", { pageSize: page_size, page: page }
    end

    def membership_documents(params = {})
      get "#{@host_url}/admin/athletes/memberships/documents", params
    end

    def certification_documents(params = {})
      get "#{@host_url}/athleteCertificates/pricingAndDocuments", params
    end

    def academy_registration_documents(params = {})
      get "#{@host_url}/admin/academies/registrations/pricingAndDocuments", params
    end

    def list_countries
      get "#{@host_url}/admin/countries"
    end

    def list_belts(params = {})
      get "#{@host_url}/belts", params
    end

    def update_championship_results championship_id, division_id, payload
      post "#{@host_url}/admin/championships/championships/#{championship_id}/divisions/#{division_id}/results", payload
    end

    def update_championship_division_results championship_id, division_id, payload
      post "#{@host_url}/admin/championships/championships/#{championship_id}/divisions/#{division_id}/podium", payload
    end

    def update_championship_division_registrations championship_id, division_id, payload
      post "#{@host_url}/admin/championships/championships/#{championship_id}/divisions/#{division_id}/registrations", payload
    end

    def delete_championship_division_registrations championship_id, division_id, competitor_id
      delete "#{@host_url}/admin/championships/championships/#{championship_id}/divisions/#{division_id}/registrations/#{competitor_id}"
    end

    def get_user(user_id)
      get "#{@host_url}/admin/security/users/#{user_id}"
    end

    def get_logged_in_user_athlete(auth_token)
      @auth_token = auth_token
      get "#{@host_url}/auth/security/user/athlete"
    end

    def authenticate!
      @auth_token = parse_response(
        ::RestClient.post(
          "#{@host_url}/auth/security/oauth/token",
          {
            grant_type: 'client_credentials',
            client_id: client_id || ENV['IBJJF_API_CLIENT_ID'],
            client_secret: client_secret || ENV['IBJJF_API_CLIENT_SECRET']
          }.to_json,
          {
            content_type: :json,
            accept: :json
          }
        )
      )[:access_token]
    end

    private

    def get_paginated endpoint
      current_page = 1
      result = []

      loop do
        res = Retryable.retryable(tries: 5, sleep: 10, on: [IbjjfApi::ApiError]) do
          get(endpoint, {pageSize: PAGE_SIZE, page: current_page})
        end

        result << res[:list]

        if res[:last_page] > current_page
          current_page += 1
        else
          break
        end
      end

      result.flatten
    end

    def get(endpoint, params = [])
      endpoint += "?#{assembly_query(params)}" unless params.empty?
      call_api(:get, [endpoint])
    end

    def post(endpoint, payload)
      call_api(:post, [endpoint, payload.to_json])
    end

    def delete(endpoint)
      call_api(:delete, [endpoint])
    end

    def call_api(method, params)
      authenticate! if @auth_token.nil?

      p = params.push(default_headers.merge(authorization_header))

      begin
        retries ||= 0

        ::RestClient.send(method, *p) do |response, request|

          case response.code
            when 200
              parse_response(response)
            when 201
              response
            when 400, 502, 500, 504
              Rails.logger.tagged("IBJJF::Client##{method}") do
                Rails.logger.info 'Invalid unhandled API response'
                Rails.logger.info "Request: #{request.uri}"
                Rails.logger.info "Response code: #{response.code}"
                Rails.logger.info "Response body: #{response.body}"
                Rails.logger.info "Params: #{params}"
              end if defined?(Rails)

              raise IbjjfApi::ApiError.new(response.code, 'Internal server error')
            when 403
              raise IbjjfApi::ForbiddenError.new

              # raise 'IBJJF::Client - Invalid unhandled API response'
          end
        end
      rescue IbjjfApi::ForbiddenError
        authenticate!

        retry if (retries += 1) <= 1

        raise
      end
    end

    def parse_response(response)
      unless response.body.empty?
        begin
          JSON.parse(response.body, symbolize_names: true)
        rescue JSON::ParserError
          response.body
        end
      end
    end

    def default_headers
      {
        content_type: :json,
        accept: :json
      }
    end

    def authorization_header
      {
        authorization: "Bearer #{@auth_token}"
      }
    end

    def assembly_query(params)
      uri = Addressable::URI.new
      uri.query_values = params
      uri.query
    end
  end
end
