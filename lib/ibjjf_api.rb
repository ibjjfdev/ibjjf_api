require "ibjjf_api/version"

module IbjjfApi
  autoload :ApiError, 'ibjjf_api/api_error'
  autoload :ForbiddenError, 'ibjjf_api/forbidden_error'

  autoload :Client, 'ibjjf_api/client'
end
