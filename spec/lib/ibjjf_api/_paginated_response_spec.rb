
RSpec.shared_examples "paginated_response" do |response|
  it "is an object" do
    expect(response).to be_a Hash
  end

  it "has a list of objects" do
    expect(response).to have_key :list
  end
end
