require 'spec_helper'
require 'pry'

RSpec.shared_examples "paginated response" do
  it "is an object" do
    expect(response).to be_a Hash
  end

  it "has a list of objects" do
    expect(response).to have_key :list
  end

  it "has pagination fields" do
    expect(response).to have_key :totalCount
    expect(response).to have_key :pageSize
    expect(response).to have_key :offset
    expect(response).to have_key :to
    expect(response).to have_key :last_page
    expect(response).to have_key :page
    expect(response).to have_key :from
  end
end

describe IbjjfApi::Client do
  subject do
    IbjjfApi::Client.new(
      host_url: 'https://hmg-api.ibjjfdb.com',
      client_id: 'D5846DC0-06A0-11E8-B136-2391F689CB12',
      client_secret: 'xizclf33uZeWzXqWQyryNWghCekO4SPprRgC4dsbc'
    )
  end

  let(:category_id) { 989_291 }
  let(:open_class_id) { 989_481 }
  let(:tournament_id) { 878 }
  let(:competitor_id) { 27_491 }
  let(:athlete_id) { 17477 }
  let(:course_id) { 1513 }
  let(:user_id) { 1 }

  describe 'GET #list_championships' do
    let(:response) { subject.list_championships(1, 1, filter_params) }

    context 'when filter by name' do
      let(:filter_params) { { name: 'World IBJJF Jiu-Jitsu Championship 2000' } }

      it 'the response return a list of championships where the name contains the expression filtered' do
        response[:list].each do |championship|
          expect(championship[:name]).to include(filter_params[:name])
        end
      end
    end

    context 'when filter by status' do
      let(:filter_params) { { name: 'active' } }

      it 'the response return a list of active championships' do
        response[:list].each do |championship|
          expect(championship[:status]).to eq('active')
        end
      end
    end

    context 'when filter by the date of championship start and finish' do
      let(:filter_params) { { dateStart: '2019-01-01', dateEnd: '2019-01-31' } }

      it 'the response return a list of championships inside the filtered interval of dates' do
        response[:list].each do |championship|
          expect(championship[:date][:start][0..9]).to be >= filter_params[:dateStart]
          expect(championship[:date][:end][0..9]).to be <= filter_params[:dateEnd]
        end
      end
    end

    context 'when filter by registration status' do
      let(:filter_params) { { registrationStatus: 'active' } }
      let(:registration_period_ends_at) do
        Time.strptime(response[:list].first.dig(:registrationPeriod, :end), '%Y-%m-%dT%H:%M:%S')
      end

      it 'the response list contains only championships open for subscriptions' do
        expect(registration_period_ends_at).to be >= Time.now
      end
    end

    it_behaves_like 'paginated response' do
      let(:response) { subject.list_championships }
    end
  end

  describe '#GET list_athletes' do
    it_behaves_like 'paginated response' do
      let(:response) { subject.list_athletes }
    end
  end

  describe '#GET get_athlete' do
    let(:response) { subject.get_athlete(athlete_id) }
    let(:fields) do
      %i[
          id createdAt updatedAt name email hallOfFame rankingBlocked
          gender academyId federationId birthDate membershipPhotoUpdatedAt
          membershipStatus certificateStatus canBeProfessor provisionalPeriodStatus
          belt academy nationality photo rankingPhoto hallOfFamePhoto
          age ageInYear canBeHeadProfessor
        ]
    end

    it 'return a hash' do
      expect(response.class).to be(Hash)
    end

    it 'has a list of fields' do
      fields.each do |field|
        expect(response).to have_key field
      end
    end
  end

  describe '#GET list_academies' do
    it_behaves_like 'paginated response' do
      let(:response) { subject.list_academies }
    end
  end

  describe '#GET active_registration_academies' do
    it_behaves_like 'paginated response' do
      let(:response) { subject.active_registration_academies }
    end
  end

  describe '#GET get_academy' do
    let(:academy_id) { 7332 }
    let(:response) { subject.get_academy(academy_id) }
    let(:fields) do
      %i[id createdAt updatedAt name status businessName email site addressLine1 addressLine2
         addressCity addressState addressZipCode mailAddressLine1 mailAddressLine2 mailAddressCity
         mailAddressState mailAddressZipCode phoneNumber logoPhotoType federation addressCountry
         mailAddressCountry association responsible professors academyRegistrationLastRequested
         academyRegistrationLastApproved registrationStatus]
    end

    it 'has a list of fields' do
      fields.each do |field|
        expect(response).to have_key field
      end
    end
  end

  describe '#GET get_academy_team' do
    let(:academy_team_id) { 664 }
    let(:response) { subject.get_academy_team(academy_team_id) }
    let(:fields) do
      %i[id createdAt updatedAt name status academy]
    end

    it 'has a list of fields' do
      fields.each do |field|
        expect(response).to have_key field
      end
    end
  end

  describe 'GET #list_championship_competitors' do
    let(:filter_params) { { beltsDivisions: 9, ageDivisions: 15, weightDivisions: '9,10,11', genders: 0 } }

    context 'when filter params was informed' do
      let(:response) { subject.list_championship_competitors(tournament_id, 1, 1, filter_params) }
      let(:division) { response[:list].first[:division] }

      it 'the response list contains only a specific gender' do
        expect(division[:gender][:id]).to eq(0)
      end

      it 'the response list contains only a specific age division' do
        expect(division[:beltDivision][:id]).to eq(9)
      end

      it 'the response list contains only a specific weight division' do
        expect(division[:weightDivision][:id]).to eq(9)
      end

      it 'the response list contains only a specific belt' do
        expect(division[:ageDivision][:id]).to eq(15)
      end
    end

    it_behaves_like "paginated response" do
      let(:response) { subject.list_championship_competitors(tournament_id) }
    end
  end

  describe "GET #list_championship_coaches" do
    it_behaves_like "paginated response" do
      let(:response) { subject.list_championship_coaches(tournament_id) }
    end
  end

  describe "GET #list_championship_divisions" do
    it_behaves_like "paginated response" do
      let(:response) { subject.list_championship_divisions(tournament_id) }
    end
  end

  describe "GET #list_championship_divisions_tree" do
    let(:response) { subject.list_championship_divisions_tree(tournament_id) }

    it 'is an array' do
      expect(response).to be_an Array
    end

    it 'has a list of fields' do
      expect(response.first).to have_key :gender
      expect(response.first).to have_key :ageDivisionList
    end

    context 'ageDivisionList fields' do
      let(:age_division_list_field ) { response.first.dig(:ageDivisionList).first }

      it 'has a list of fields' do
        expect(age_division_list_field).to have_key :ageDivision
        expect(age_division_list_field).to have_key :birthYearMin
        expect(age_division_list_field).to have_key :birthYearMax
        expect(age_division_list_field).to have_key :beltDivisionList
      end
    end
  end

  describe "GET #list_championship_results" do
    it_behaves_like "paginated response" do
      let(:response) { subject.list_championship_results(tournament_id) }
    end
  end

  describe "GET #list_championship_prices" do
    let(:response) { subject.list_championship_prices(tournament_id) }

    it 'is an array' do
      expect(response).to be_an Array
    end

    it 'has a list of fields' do
      expect(response.first).to have_key :id
      expect(response.first).to have_key :name
      expect(response.first).to have_key :values
    end
  end

  describe "list_championship_prizes" do
    context 'for the normal type of prizes' do
      let(:tournament_id) { 1423 }
      let(:response) { subject.list_championship_prizes(tournament_id) }

      it 'is a hash' do
        expect(response).to be_an Hash
      end

      it 'has a list of fields' do
        expect(response).to have_key :type
        expect(response).to have_key :items
      end

      context 'the items key' do
        let(:items) { response[:items] }
        let(:item) { items.first }

        it 'is an array' do
          expect(items).to be_an Array
        end

        it 'has a list of fields' do
          expect(item).to have_key :title
          expect(item).to have_key :description
          expect(item).to have_key :descriptionLabel
          expect(item).to have_key :prizeLabel
          expect(item).to have_key :prizes
        end
      end

      context 'the prizes key' do
        let(:prizes) { response[:items].first[:prizes] }
        let(:prize) { prizes.first }

        it 'is an array' do
          expect(prizes).to be_an Array
        end

        it 'has a list of fields' do
          expect(prize).to have_key :title
          expect(prize).to have_key :description
          expect(prize).to have_key :place01Label
          expect(prize).to have_key :place02Label
          expect(prize).to have_key :place03Label
          expect(prize).to have_key :place04Label
          expect(prize).to have_key :place01Value
          expect(prize).to have_key :place02Value
          expect(prize).to have_key :place03Value
          expect(prize).to have_key :place04Value
        end
      end
    end

    context 'for the mundial type of prizes' do
      let(:tournament_id) { 1509 }
      let(:response) { subject.list_championship_prizes(tournament_id) }

      it 'is a hash' do
        expect(response).to be_an Hash
      end

      it 'has a list of fields' do
        expect(response).to have_key :type
        expect(response).to have_key :items
      end

      context 'the items key' do
        let(:items) { response[:items] }
        let(:item) { items.first }

        it 'is an array' do
          expect(items).to be_an Array
        end

        it 'has a list of fields' do
          expect(item).to have_key :title
          expect(item).to have_key :description
          expect(item).to have_key :place01Label
          expect(item).to have_key :place02Label
          expect(item).to have_key :place03Label
          expect(item).to have_key :place04Label
          expect(item).to have_key :prizes
        end
      end

      context 'the prizes key' do
        let(:prizes) { response[:items].first[:prizes] }
        let(:prize) { prizes.first }

        it 'is an array' do
          expect(prizes).to be_an Array
        end

        it 'has a list of fields' do
          expect(prize).to have_key :description
          expect(prize).to have_key :place01
          expect(prize).to have_key :place02
        end
      end
    end
  end

  describe "list_course_prices" do
    let(:response) { subject.list_course_prices(course_id) }

    it 'is an array' do
      expect(response).to be_an Array
    end

    it 'has a list of fields' do
      expect(response.first).to have_key :id
      expect(response.first).to have_key :name
      expect(response.first).to have_key :values
    end
  end

  describe "GET #list_championship_academies_results" do
    it_behaves_like "paginated response" do
      let(:response) { subject.list_championship_academies_results(tournament_id) }
    end
  end

  describe "GET #championship_info" do
    let(:response) { subject.championship_info(tournament_id) }
    let(:championship_name) { 'Mexico City Summer International Open IBJJF Jiu-Jitsu Championship 2018' }

    it 'is an object' do
      expect(response).to be_a Hash
    end

    it 'has a list of fields' do
      expect(response).to have_key :id
      expect(response).to have_key :name
      expect(response).to have_key :federation
      expect(response).to have_key :timezone
      expect(response).to have_key :date
      expect(response).to have_key :registrationChangeByProfessorPeriod
      expect(response).to have_key :registrationChangeByAthleteDateEnd
      expect(response).to have_key :checkDate
      expect(response).to have_key :notificationsEnabled
      expect(response).to have_key :registrationChangeByProfessorStatus
      expect(response).to have_key :status
    end

    it 'return a specific championship' do
      expect(response[:name]).to eq(championship_name)
    end
  end

  describe 'GET #membership_documents' do
    let(:docs) do
      { countryCode: 'US', birthDate: '1993-11-27', beltColor: 'black', membershipType: 'first' }
    end

    let(:response) { subject.membership_documents(docs) }
    let(:required_docs) { %w[membershipFormAdult photo identification beltRecords] }

    it 'returns an array' do
      expect(response).to be_a(Hash)
    end

    it 'return a specific list of documents' do
      required_docs.each do |required_doc|
        expect(response[:documents]).to include(required_doc)
      end
    end

    it 'return the membership fee' do
      expect(response[:price]).to eq(40)
    end

    it 'return the currency of membership fee' do
      expect(response[:currency]).to eq('USD')
    end

    context 'with invalid params' do
      let(:docs) do
        { countryCode: 'AA', birthDate: '1993-11-27', beltColor: 'black', membershipType: '' }
      end

      it 'return an error' do
        expect { response }.to raise_error(IbjjfApi::ApiError)
      end
    end
  end

  describe 'GET #certification_documents' do
    let(:params) do
      { isFirst: true, countryCode: 'DE' }
    end

    let(:response) { subject.certification_documents(params) }
    let(:required_docs) { %w[firstAid rulesSeminar] }

    it 'returns an array' do
      expect(response).to be_a(Hash)
    end

    it 'return a specific list of documents' do
      required_docs.each do |required_doc|
        expect(response[:documents]).to include(required_doc)
      end
    end

    it 'return the membership fee' do
      expect(response[:price]).to eq(400)
    end

    it 'return the currency of membership fee' do
      expect(response[:currency]).to eq('USD')
    end

    context 'with invalid params' do
      let(:params) do
        { isFirst: '', countryCode: 'US' }
      end

      it 'return an error' do
        expect { response }.to raise_error(IbjjfApi::ApiError)
      end
    end
  end

  describe 'GET #academy_registration_documents' do
    let(:params) do
      { countryCode: 'US', hasAssistantProfessors: true, belongsToAssociation: true }
    end

    let(:response) { subject.academy_registration_documents(params) }
    let(:required_docs) { %w[associationApproval assistantProfessorsApproval backgroundCheck] }

    it 'returns an array' do
      expect(response).to be_a(Hash)
    end

    it 'return a specific list of documents' do
      required_docs.each do |required_doc|
        expect(response[:documents]).to include(required_doc)
      end
    end

    it 'return the membership fee' do
      expect(response[:price]).to eq(70)
    end

    it 'return the currency of membership fee' do
      expect(response[:currency]).to eq('USD')
    end

    context 'with invalid params' do
      let(:params) do
        { countryCode: 'US', hasAssistantProfessors: true }
      end

      it 'return an error' do
        expect { response }.to raise_error(IbjjfApi::ApiError)
      end
    end
  end

  describe 'GET #list_countries' do
    let(:response) { subject.list_countries }

    it 'return an array' do
      expect(response).to be_an(Array)
    end
  end

  describe '#list_belts' do
    let(:response) { subject.list_belts }

    it 'return an array' do
      expect(response).to be_an(Array)
    end
  end

  describe '#list_courses' do
    let(:response) { subject.list_courses }

    it_behaves_like 'paginated response' do
      let(:response) { subject.list_championships }
    end
  end

  describe '#get_course' do
    let(:response) { subject.get_course(course_id) }
    let(:fields) do
      %i[
          id name local localAddressLine1 localAddressLine2
          localAddressLine3 localAddressCity localAddressState
          localAddressZipCode paymentDateLimit releaseDate timezone
          logoColor date registrationPeriod logo registrationStatus status
        ]
    end

    it 'return a hash' do
      expect(response.class).to be(Hash)
    end

    it 'has a list of fields' do
      fields.each do |field|
        expect(response).to have_key field
      end
    end
  end

  describe 'POST #update_championship_results' do
    let(:response) do
      subject.update_championship_results(
        tournament_id,
        category_id,
        [
          { athleteId: 61, position: 1, status: "normal" },
          { athleteId: 27491, position: 2, status: "normal" }
        ]
      )
    end

    it "return a specific http code" do
      expect(response.code).to eq(201)
    end
  end

  describe 'POST #update_championship_division_results' do
    let(:response) do
      subject.update_championship_division_results(
        tournament_id,
        category_id,
        [
          { athleteId: 61, checkin: true, deliveredTo: "athleteMissing", deliveredToName: "", deliveredToLink: "", deliveredToContact: "", note: "" },
          { athleteId: 27491, checkin: true, deliveredTo: "athlete", deliveredToName: "", deliveredToLink: "", deliveredToContact: "", note: ""  }
        ]
      )
    end

    it "return a specific http code" do
      expect(response.code).to eq(201)
    end
  end

  describe 'POST #update_championship_division_registrations' do
    let(:response) do
      subject.update_championship_division_registrations(
        tournament_id,
        open_class_id,
        { athleteId: competitor_id, academyTeamId: 940  }
      )
    end

    it "return a specific http code" do
      expect(response.code).to eq(201)
    end
  end

  describe 'DELETE #delete_championship_division_registrations' do
    let(:response) do
      subject.delete_championship_division_registrations(
        tournament_id,
        open_class_id,
        competitor_id
      )
    end

    it "return a specific success message" do
      expect(response).to eq({ ok: true })
    end
  end

  describe '#GET get_user' do
    let(:response) { subject.get_user(user_id) }
    let(:fields) do
      %i[id createdAt updatedAt email athlete federation groups associations permissions]
    end

    it 'return a hash' do
      expect(response.class).to be(Hash)
    end

    it 'has a list of fields' do
      fields.each do |field|
        expect(response).to have_key field
      end
    end
  end

  describe '#GET get_logged_in_user_athlete' do
    let(:token) do
      res = ::RestClient.post(
        "https://core-staging.ibjjf.com/users/login",
        {
          user: {
            email: 'user-1@ibjjf.com',
            password: 'senha'
          }
        },
        {
          content_type: 'application/x-www-form-urlencoded',
        }
      )
      JSON.parse(res.body, symbolize_names: true)[:data][:attributes][:token]
    end
    let(:response) { subject.get_logged_in_user_athlete(token) }
    let(:fields) do
      %i[id createdAt updatedAt name email hallOfFame gender beltId academyId professorId
         federationId birthDate addressLine1 addressLine2 addressCity addressState addressZipCode
         lastApprovedMembershipId lastRequestedMembershipId membershipStatus membershipPhotoUpdatedAt
         certificateStatus canBeProfessor provisionalPeriodStatus provisionalPeriodStartAt
         provisionalPeriodEndAt rankingBlocked userId academy belt federation lastApprovedMembership
         lastRequestedMembership membership photo]
    end

    it 'return a hash' do
      expect(response.class).to be(Hash)
    end

    it 'has a list of fields' do
      fields.each do |field|
        expect(response[:athlete]).to have_key field
      end
    end
  end
end
